require('dotenv').config()
const express = require('express');
const Redis = require('ioredis');
const app = express();
const {validateRequest} = require('./handler/rate_limiter');
const windowSizeSeconds=process.env.WINDOW_SIZE_IN_SECONDS;
const capacity=process.env.CAPACITY;
const redisConfig={
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
}
const redis = new Redis(redisConfig.host,redisConfig.port);
app.use((req,res,next)=>{
    validateRequest(req,res,next,redis,windowSizeSeconds,capacity)
})
app.get("/ping",(req,res,next)=>{
    res.send("pong");
    next();
})
app.listen(process.env.PORT || 3000,()=>{
    console.log(`App is listening on port ${PORT}`)
})