const moment = require('moment');

function validateRequest(request, response, next, redis, windowSizeSeconds, capacity){
    redis.get(request.ip,(err,record)=>{
        if (err) throw err;
        let currentTime = moment().toISOString();
        if (!record){
            let requestLog={
                lastResetTime: currentTime,
                tokenBucket: capacity
            }
            console.log(`CREATE NEW RECORD for ${request.ip}`)
            redis.set(request.ip,JSON.stringify(requestLog))
            next();
        }
        // If have record
        else {
            let data = JSON.parse(record);
            if (record && moment(currentTime).diff(moment(data.lastResetTime),'seconds') >= windowSizeSeconds){
                data={lastResetTime: currentTime, tokenBucket: capacity}
                console.log(`RESET TOKEN OF ${request.ip} AT: ${data.lastResetTime}`)
                redis.set(request.ip,JSON.stringify(data));
                next();
            }
            else {
                console.log(`REDUCING TOKEN OF ${request.ip} TO: ${data.tokenBucket -1}`)
                data.tokenBucket = data.tokenBucket - 1;
                if (data.tokenBucket <= 0 ){
                    console.log(`${request.ip} OUT OF TOKEN AT ${currentTime}`)
                    response.status(429).end(`Excessed limit of ${capacity} request per ${windowSizeSeconds} seconds`);
                }
                else {
                    data.lastResetTime = moment(currentTime)
                    redis.set(request.ip,JSON.stringify(data));
                    next();
                }
            }
        }

    })
}

exports.validateRequest=validateRequest;