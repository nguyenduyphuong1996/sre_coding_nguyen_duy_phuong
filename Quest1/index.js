const fs = require('fs');
const moment = require('moment');

const WINDOW_SIZE_IN_SECONDS=3600;
let inputFilePath = process.argv[2];

function validateRequest(requestTimeStamps, windowSizeSeconds, capacity){
    let lastResetTime=0;
    requestTimeStamps.forEach((record)=>{
            console.log("LAST RESET TIME: ", lastResetTime)
            console.log("TIME DISTANCE: ", moment(record).diff(moment(lastResetTime),'seconds'))
            if (moment(record).diff(moment(lastResetTime),'seconds') >= windowSizeSeconds){
                lastResetTime = moment(record)
                tokens = capacity;
                console.log("token resetted")
                console.log("true");
            }
            else {
                tokens = tokens - 1;
                if (tokens <= 0){
                    console.log("out of token")
                    console.log("false")
                }
                else {
                    console.log("token decreased")
                    lastResetTime = moment(record)
                    console.log("true")
                }
            }
    })
    
}
async function main(){
    await fs.readFile(inputFilePath,'utf8',(err,data)=>{
        if (err) throw err;
        let inputArr=data.split(/\r?\n/ );
        let capacity=inputArr[0].split(" ")[1];
        let timeStamps=inputArr.slice(1);
        validateRequest(timeStamps,WINDOW_SIZE_IN_SECONDS,capacity)
    })

}

main();